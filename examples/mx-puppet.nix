{ config, lib, pkgs, ... }:

{
  services.matrix-appservices = {
    addRegistrationFiles = true;

    services = {
      /** Prefer mautrix-discord
      discord = {
        port = 29180;
        format = "mx-puppet";
        package = pkgs.mx-puppet-discord;
        settings.bridge.enableGroupSync = true;
      };
      */
      slack = {
        port = 29182;
        format = "mx-puppet";
        package = pkgs.mx-puppet-slack;
      };

      /** groupme is broken, read pkgs/default.nix
      groupme = {
        port = 29181;
        format = "mx-puppet";
        package = pkgs.mx-puppet-groupme;
      };
      */
    };
  };
}
