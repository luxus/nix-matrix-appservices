{ system ? builtins.currentSystem
, inputs ? (import ./.).inputs
}:
let
  pkgs = inputs.nixpkgs.legacyPackages.${system};

  homeserverURL = "http://homeserver:8008";
  homeserverDomain = "myserver.com";

  mkBotTest = botUserName:
    pkgs.writers.writePython3Bin "test_${botUserName}"
    { libraries = [ pkgs.python3Packages.matrix-nio ]; }
    ''
      import asyncio

      from datetime import datetime
      from nio import AsyncClient


      botUserID = "@${botUserName}:${homeserverDomain}"

      # How long to wait for events before quiting
      timeout = 3  # in Seconds


      def timeoutError(goal):
          exit(f"{botUserID} exceeded the timeout of {timeout} seconds to {goal}")


      async def main() -> None:
          # Connect to dendrite
          client = AsyncClient("${homeserverURL}", "alice")

          # Register as user alice
          await client.register("alice", "my-secret-password")

          # Log in as user alice
          await client.login("my-secret-password")

          # Create a new room
          room = await client.room_create(federate=False)
          room_id = room.room_id

          # Join the room
          await client.join(room_id)

          # Invite bot user to room
          await client.room_invite(
            room_id,
            botUserID
          )

          ctime = datetime.now()
          while True:
              if (datetime.now() - ctime).seconds > timeout:
                  timeoutError("join the room")
              members = (await client.joined_members(room_id)).members
              # BREAK HERE once bot user has joined the room
              if len(members) > 1:
                  break

          await client.room_send(
              room_id=room_id,
              message_type="m.room.message",
              content={
                  "msgtype": "m.text",
                  "body": "help"
              }
          )

          ctime = datetime.now()
          while True:
              if (datetime.now()-ctime).seconds > timeout:
                  timeoutError("respond to a help message")

              syncResponse = await client.sync(timeout=30000)
              lastEvent = syncResponse.rooms.join[room_id].timeline.events[-1]
              # BREAK HERE once bot user has sent an event to a room
              # Should mean that bot has responded to help message
              if lastEvent.sender == botUserID:
                  break

          # Leave the room
          await client.room_leave(room_id)

          # Close the client
          await client.close()

      asyncio.get_event_loop().run_until_complete(main())
    '';

  botUsersToTest = [
    "whatsappbot"
    "discordbot"
    # TODO: fix signald startup in checks
    # "signalbot"
    "facebookbot"
    "twitterbot"
    "instagrambot"
    "_slackpuppet_bot"

  ];

  botTests = map mkBotTest botUsersToTest;
  botTestCommands = map (t: "client.succeed(\"${t.name}\")") botTests;

  private_key = pkgs.runCommand "matrix_key.pem" {
    buildInputs = [ pkgs.dendrite ];
  } "generate-keys --private-key $out";

  appserviceTest = pkgs.nixosTest {
    name = "matrix-appservices";

    nodes = {
      homeserver = { pkgs, ... }: {
        imports = [
          ./modules/matrix-appservices
          ./examples/mautrix.nix
          ./examples/mx-puppet.nix
        ];
        nixpkgs.overlays = [ inputs.self.overlays.default ];
        services.dendrite = {
          enable = true;
          openRegistration = true;
          settings = {
            global.server_name = homeserverDomain;
            global.private_key = private_key;
            client_api.registration_disabled = false;
          };
        };

        networking.firewall.allowedTCPPorts = [ 8008 ];
    
        services.matrix-appservices = {
          inherit homeserverDomain homeserverURL;
          addRegistrationFiles = true;
          homeserver = "dendrite";
        };
      };
    
      client = { pkgs, ... }: {
        environment.systemPackages = botTests;
      };
    };
    
    testScript = ''
      start_all()
    
      with subtest("start the homeserver"):
          homeserver.wait_for_unit("dendrite.service")
          homeserver.wait_for_open_port(8008)
          homeserver.wait_for_unit("matrix-as.target")
    
      with subtest("ensure messages can be exchanged"):
          ${builtins.concatStringsSep "\n" botTestCommands}
    '';
    
  };
in
{
  matrix-appservices = appserviceTest;
}
