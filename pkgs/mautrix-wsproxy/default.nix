{ lib, sources, buildGoModule, fetchFromGitHub, olm }:

buildGoModule rec {
  inherit (sources.mautrix-wsproxy) pname version src;

  buildInputs = [ olm ];

  vendorSha256 = "sha256-xFA06e252lKm8aivpSR/1weuJ/oZOM0ZhiCv/v5FL0s=";

  doCheck = false;

  proxyVendor = true;

  postInstall = ''
    mv $out/bin/mautrix-wsproxy $out/bin/mautrix-wsproxy-bin

  '';

  meta = with lib; {
    homepage = "https://github.com/mautrix/wsproxy";
    description = "A simple HTTP push -> websocket proxy for Matrix appservices.";
    mainProgram = "mautrix-wsproxy-bin";
    license = licenses.agpl3Plus;
    maintainers = with maintainers; [ pacman99 ];
  };
}
