{ stdenv
, lib
, python3
, makeWrapper
, fetchFromGitHub
, sources
}:

with python3.pkgs;

let
  # officially supported database drivers
  dbDrivers = [
    asyncpg
    # sqlite driver is already shipped with python by default
  ];

in

buildPythonApplication rec {
  inherit (sources.mautrix-instagram) pname version src;

  postPatch = ''
    sed -i -e '/alembic>/d' requirements.txt
  '';
  postFixup = ''
    makeWrapper ${python}/bin/python $out/bin/mautrix-instagram \
      --add-flags "-m mautrix_instagram" \
      --prefix PYTHONPATH : "$(toPythonPath ${mautrix}):$(toPythonPath $out):$PYTHONPATH"
  '';

  propagatedBuildInputs = [
    mautrix
    yarl
    aiohttp
    aiosqlite
    beautifulsoup4
    sqlalchemy
    CommonMark
    ruamel_yaml
    paho-mqtt
    python_magic
    attrs
    pillow
    qrcode
    phonenumbers
    pycryptodome
    python-olm
    unpaddedbase64
    setuptools
  ] ++ dbDrivers;

  checkInputs = [
    pytest
    pytestrunner
    pytest-mock
    pytest-asyncio
  ];

  doCheck = false;

  meta = with lib; {
    homepage = "https://github.com/tulir/mautrix-instagram";
    description = "A Matrix-Instagram DM puppeting bridge";
    license = licenses.agpl3Plus;
    platforms = platforms.linux;
  };

}

