{ pkgs }:
let
  sources = callPackage ./_sources/generated.nix { };

  mautrix-python = pkgs.python3.pkgs.callPackage ./mautrix-python {
    inherit sources;
  };

  callPackage = pkgs.lib.callPackageWith (pkgs // {
    inherit sources;
    python3 = pkgs.python3.override {
      packageOverrides = final: prev: {
        mautrix = mautrix-python;
      };
    };
  });
in
{
  mx-puppet-slack = callPackage ./mx-puppet-slack { };
  # groupme uses an outdated version of better-sqlite3 that doesn't compile in 22.05-pre
  # mx-puppet-groupme = callPackage ./mx-puppet-groupme { };

  mautrix-discord = callPackage ./mautrix-discord { };
  mautrix-twitter = callPackage ./mautrix-twitter { };
  mautrix-instagram = callPackage ./mautrix-instagram { };
  mautrix-wsproxy = callPackage ./mautrix-wsproxy { };
  mautrix-imessage = callPackage ./mautrix-imessage { };
  mautrix-syncproxy = callPackage ./mautrix-syncproxy { };
}
