{ lib, buildGoModule, fetchFromGitHub, olm, sources }:

buildGoModule rec {
  inherit (sources.mautrix-syncproxy) pname version src;

  vendorSha256 = "sha256-+0HNyVrUO7Lo/VSbrm0mgWlMYIpfvq8DzgaQhrvcdgw=";

  doCheck = false;

  proxyVendor = true;

  postInstall = ''
    mv $out/bin/mautrix-syncproxy $out/bin/mautrix-syncproxy-bin
  '';

  meta = with lib; {
    homepage = "https://github.com/mautrix/syncproxy";
    description = "A /sync proxy for encrypted Matrix appservices.";
    mainProgram = "mautrix-syncproxy-bin";
    license = licenses.agpl3Plus;
    maintainers = with maintainers; [ ];
  };
}
