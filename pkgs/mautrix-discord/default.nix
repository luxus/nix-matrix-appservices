{ lib, buildGoModule, fetchFromGitHub, olm, sources }:

buildGoModule rec {
  inherit (sources.mautrix-syncproxy) pname version src;

  vendorSha256 = "sha256-+xQO0ClEMEPLK2OjlV0hn641eWYgoxYXHNQCJet3i+g=";

  doCheck = false;

  proxyVendor = true;

  postInstall = ''
    mv $out/bin/mautrix-discord $out/bin/mautrix-discord-bin
  '';

  meta = with lib; {
    homepage = "https://github.com/mautrix/discord";
    description = "Matrix <-> Discord hybrid puppeting/relaybot bridge";
    license = licenses.agpl3;
    mainProgram = "mautrix-discord-bin";
    maintainers = with maintainers; [ pacman99 ];
  };
}
